**User Management functions**

This repository provides user management functions required for bluemarble. 
The identity provider integrated in this repository is **Keycloak**. 
The service is created in such a way that additional identity providers can be plugged-in (by way of writing specific providers/adapters).
The repository also has a mock identity provider which can be used for quick testing.

---

## Application.properties details

This is a springboot application. The application.properties file holds certain important configurations.

1. **idp.provider** - The idp provider that needs to be integrated to perform user management functions. *keycloak and mock are the values currently supported.* 
2. **server.url** - The url of the idp server. For keycloak it is typically *https://<host:port>/auth*
3. **admin.realm** - A realm manages a set of users, credentials, roles, and groups. A user belongs to and logs into a realm. Realms are isolated from one another and can only manage and authenticate the users that they control. This is a **keycloak** concept. Similar constructs could be there in other identity providers.  
4. **admin.user** - An admin user who has the permissions to create and manage users.
5. **admin.password** - The password of the admin user.

---

## Endpoint details

The following endpoints are supported

1. **/usermanagement/anonymous** - This endpoint provides access and refresh tokens for an anonymous user. 
2. **/usermanagement/refresh** - This endpoint provides an access token from a valid refresh token.
3. **/usermanagement/register** - This endpoint registers/adds a new user and assigns the provided roles to that user.

---

## Configurations to be passed in the endpoints

Each of the above endpoints requires certain configurations to be passed.

For **Keycloak** provider, the following configurations are a must:

1. **user.realm** - The realm to which a user belongs to.
2. **client.id** - Clients are entities that can request Keycloak to authenticate a user.

